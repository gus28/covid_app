package com.example.covidprueba;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Ayuda extends AppCompatActivity {

    private TextView telefono1, telefono2;
    private ImageButton tel1;
    private EditText editTextPhone;

    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda);

        getSupportActionBar().hide();

        telefono1 = (TextView) findViewById(R.id.tv_tel1);
        telefono2 = (TextView) findViewById(R.id.tv_tel2);

        tel1 = (ImageButton) findViewById(R.id.ib_t1);
        editTextPhone = (EditText)findViewById(R.id.et_phone);


        tel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String phoneNumber = editTextPhone.getText().toString();

                if (phoneNumber != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                    } else {
                        OlderVersions(phoneNumber);
                    }
                }

            }
        });

        telefono1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String telef1 = telefono1.getText().toString();

                editTextPhone.setText(telef1);



            }
        });

        telefono2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String telef2 = "55 56 58 11 11";

                editTextPhone.setText(telef2);


            }
        });

    }

    private void OlderVersions(String phoneNumber){
        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));

        int result = checkCallingOrSelfPermission(Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED){

            startActivity(intentCall);

        }else{
            Toast.makeText(Ayuda.this,"Acceso no autorizado", Toast.LENGTH_LONG).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResult){
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResult[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){
                    if (result == PackageManager.PERMISSION_GRANTED){
                        String phoneNumber = editTextPhone.getText().toString();
                        Intent intentCall = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                        if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;;
                        startActivity(intentCall);
                    }else {
                        Toast.makeText(Ayuda.this,"Acceso no autorizado", Toast.LENGTH_LONG).show();
                    }
                }

        }

    }
}