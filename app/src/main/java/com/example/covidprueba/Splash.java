package com.example.covidprueba;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;

public class Splash extends AppCompatActivity {

    private final int DURACION_SPLASH = 3000;
    public TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();



        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(Splash.this, Comenzar.class);
                startActivity(intent);
                finish();

            };
        }, DURACION_SPLASH);

        Texto();
    }

    public void Texto(){

        text = (TextView)findViewById(R.id.tv_texto);
        final String txtR[]={
          "Lavate las manos de manera continua",
          "No salgas si no es necesario",
          "Usa cubrebocas cuando sea necesario salir",
          "Manten una distacia de 1.5 metros de otra persona",
          "#QuedateEnCasa"
        };

        Random random = new Random();
        int txt1 = random.nextInt(5);

        text.setText(txtR[txt1]);

    }
}